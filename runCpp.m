close all
clear all
clc

%system('make clean');   % (optional): clean targets
%system('bash driver');  % execute .cpp code to generate data
addpath './Data'

data1 = data3d % get .cpp generated data
data2 = e1p2data % get .cpp generated data


C=data1.C0;


[rows cols slices] = size(C)

x = linspace(0,2*pi,rows);
y = linspace(0,2*pi,cols);
z = linspace(0,2*pi,slices);

[xx yy zz] = meshgrid(x,y,z);
xx=reshape(xx,rows*cols*slices,1);
yy=reshape(yy,rows*cols*slices,1);
zz=reshape(zz,rows*cols*slices,1);
C =reshape(C,rows*cols*slices,1);
figure
S=scatter3(xx,yy,zz,(abs(C)*10).^2,C,'filled');
axis tight
axis equal
disp('Done')