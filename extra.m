close all
clear all
clc

%system('make clean');   % (optional): clean targets
system('bash driver');  % execute .cpp code to generate data
addpath './Data'

data = e2p1_data

%% Formatting
theta   = data.theta;
x       = data.x';
y       = data.y';

%% Plotting
    close all
    figure
    hold on
    %surf(x,y,zeros(size(theta)))
    surf(x,y,theta,'LineStyle','none')
    colorbar

    axis tight
    view(2)
    xlabel 'x'
    ylabel 'y'
    zlabel '\theta'