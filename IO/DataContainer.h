#pragma once
#include <armadillo>
#include <iostream>
#include <fstream>

using namespace std;
using namespace arma;

class DataContainer{
public:
  DataContainer():
    nm(0),nv(0),nc(0)
  {}

  void saveMatrix(mat m,std::string label = "M"){
    matData.push_back(m);
    if (label == "M") {
      label + std::to_string(nm);
    }
    matLabels.push_back(label);
    nm++;
  }

  void saveVector(vec v,std::string label = "V"){
    vecData.push_back(v);
    if (label == "V") {
      label += std::to_string(nv);
    }
    vecLabels.push_back(label);
    nv++;
  }
  void saveCube(cube c,std::string label = "C"){
    cubeData.push_back(c);
    if (label == "C") {
      label += std::to_string(nc);
    }
    cubeLabels.push_back(label);
    nc++;
  }

  void writeToFile(std::string fileName = "dataset"){
    // create data file
    ofstream thisFile;
    thisFile.open("Data/" + fileName + ".m");
    thisFile << "function data=" << fileName << endl;

    if (nm == 0 && nv ==0 && nc==0) {
      cout << "DataContainer: Could not write data -> was empty." << endl;
      thisFile.close();
      return;
    }
    // write data
    for (size_t i = 0; i < nm; i++) {
      // write matrices
      thisFile << "data." << matLabels.at(i) << "=[";
      mat M = matData.at(i);
      for (size_t row = 0; row < M.n_rows; row++) {
        for (size_t col = 0; col < M.n_cols; col++) {
          thisFile << M(row,col) << " ";
        }
        thisFile<<endl;
      }

      thisFile << "];" << endl;
    }
    for (size_t i = 0; i < nv; i++) {
        // write vectors
        thisFile << "data." << vecLabels.at(i) << "=[";
        vec v = vecData.at(i);
        for (size_t i = 0; i < v.n_rows; i++) {
          thisFile << v(i) << " ";
        }

        thisFile << "];" << endl;
    }

    for (size_t i = 0; i < nc; i++) {
      cube C = cubeData.at(i);
      thisFile << cubeLabels.at(i) << "=zeros("<<C.n_rows << ","<<C.n_cols<<","<<C.n_slices <<");"<<endl;
      for (size_t slice = 0; slice < C.n_slices; slice++) {
        thisFile << cubeLabels.at(i)<<"(:,:,"<<slice+1<<")=[";

        mat M = C.slice(slice);
        for (size_t row = 0; row < M.n_rows; row++) {
          for (size_t col = 0; col < M.n_cols; col++) {
            thisFile << M(row,col) << " ";
          }
          thisFile<<endl;
        }

        thisFile << "];" << endl;
      }

      thisFile << "data." << cubeLabels.at(i) << "=" << cubeLabels.at(i) <<";" << endl;
    }

    // close data file
    thisFile.close();
  }
private:
  int nm, nv, nc;
  std::vector<mat> matData;
  std::vector<cube> cubeData;
  std::vector<vec> vecData;
  std::vector<std::string> matLabels;
  std::vector<std::string> cubeLabels;
  std::vector<std::string> vecLabels;
};
