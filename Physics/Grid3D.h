
class Grid3D{
public:
  Grid3D(double Lx, double Ly, double Lz, int Nx, int Ny, int Nz):
    Lx(Lx),
    Ly(Ly),
    Lz(Lz),
    Nx(Nx),
    Ny(Ny),
    Nz(Nz){
      dx0 = Lx/double(Nx+1);
      dy0 = Ly/double(Ny+1);
      dz0 = Lz/double(Nz+1);

      dx = ones<vec>(Nx+2)*dx0; dx(0)/=2; dx(Nx+1)/2;
      dy = ones<vec>(Ny+2)*dy0; dy(0)/=2; dy(Nx+1)/2;
      dz = ones<vec>(Nz+2)*dz0; dz(0)/=2; dz(Nx+1)/2;

      x = vec(Nx); x(0)=dx0;
      y = vec(Ny); y(0)=dy0;
      z = vec(Nz); z(0)=dz0;
      for (size_t i = 1; i < Nx; i++) {
        x(i) = x(i-1)+dx(i);
      }
      for (size_t i = 1; i < Ny; i++) {
        y(i) = y(i-1)+dy(i);
      }
      for (size_t i = 1; i < Nz; i++) {
        z(i) = z(i-1)+dz(i);
      }
      dxe = vec(Nx);
      dxw = vec(Nx);
      dys = vec(Ny);
      dyn = vec(Ny);
      dzf = vec(Nz);
      dzb = vec(Nz);

      dxw(0)    = x(0);
      dxe(Nx-1) = Lx - x(Nx-1);
      for (size_t i = 1; i < Nx; i++) {
        double distance = x(i)-x(i-1);
        dxw(i)  = distance;
        dxe(i-1)= distance;
      }
      dyn(0)    = y(0);
      dys(Ny-1) = Ly - y(Ny-1);
      for (size_t i = 1; i < Ny; i++) {
        double distance = y(i)-y(i-1);
        dyn(i)  = distance;
        dys(i-1)= distance;
      }
      dzf(0)    = z(0);
      dzb(Nz-1) = Lz - z(Ny-1);
      for (size_t i = 1; i < Nz; i++) {
        double distance = z(i)-z(i-1);
        dzf(i)  = distance;
        dzb(i-1)= distance;
      }

      xe = vec(Nx);
      xw = vec(Nx);
      ys = vec(Ny);
      yn = vec(Ny);
      zf = vec(Nz);
      zb = vec(Nz);

      for (size_t i = 0; i < Nx; i++) {
        xw(i) = x(i) - dx(i+1)/2.0;
        xe(i) = x(i) + dx(i+1)/2.0;
      }
      for (size_t i = 0; i < Ny; i++) {
        yn(i) = y(i) - dy(i+1)/2.0;
        ys(i) = y(i) + dy(i+1)/2.0;
      }
      for (size_t i = 0; i < Nz; i++) {
        zf(i) = z(i) - dz(i+1)/2.0;
        zb(i) = z(i) + dz(i+1)/2.0;
      }

    }
    sp_mat getCoefficientMatrix(){return A;}
    // Fill B with rhs values for poisson eq. --> B

    template<class Function>
    auto calculateTheta(const Function& fin)->cube{
      vec b(Nx*Ny*Nz);
      mat bb(Nx,Ny);

      int nn = Nx*Ny;
      size_t i,j,k;
      // cout << "Building B" << endl;
      for (k = 0; k < Nz; k++) {
        for (i = 0; i < Nx; i++) {
          for (j = 0; j < Ny; j++) {
                bb(i,j)=-fin(xw(i),xe(i),yn(j),ys(j),zf(k),zb(k)); // integral of RHS of poisson eq.
          }
        }
        // cout << "("<<i<<","<<j<<","<<k<<")" << endl;
        b(span(nn*k,nn*(k+1)-1)) = vectorise(bb);
      }
      // solve A*theta=b --> theta
      vec theta = spsolve(A,b);
      cube Theta(Nx,Ny,Nz);
      // cout << "Reshaping" << endl;
      for (size_t i = 0; i < Nz; i++) {
        Theta.slice(i) = reshape(theta(span(i*nn,(i+1)*nn-1)),Nx,Ny);
      }
      // reshape theta and return it --> Theta
      return Theta;
    }

      void applyLaplacianOnTheta(){
        int row =0, col=-1, slice=0;
        N = Nx*Ny*Nz;
        A = sp_mat(N,N);
        for (size_t i = 0; i < N; i++) {
          if (col<Nx-1) {
            ++col;
          } else{
            ++row;
            if(row<Ny){
              col=0;
            } else{
              if(slice<Nz){
                row=0;
                col=0;
                ++slice;
              }
            }
          }
          //cout << "("<<row<<","<<col<<","<<slice<<")" << endl;
          for (size_t j = 0; j < N; j++) {
            if (i==j) {     //Diagonal ~ a_p
              A(i,j) =  dy(col+1)*dz(slice+1)*(1.0/dxe(row)+1.0/dxw(row));
              A(i,j) += dx(row+1)*dz(slice+1)*(1.0/dyn(col)+1.0/dys(col));
              A(i,j) += dx(row+1)*dy(col+1)*(1.0/dzf(slice)+1.0/dzb(slice));
            } else if (i+1==j && row!=Nx){   // East ~ a_e
              A(i,j) = -dz(slice+1)*dy(col+1)/dxe(row);
            } else if (i-1==j && row!=0){     // West ~ a_w
              A(i,j) = -dz(slice+1)*dy(col+1)/dxw(row);
            } else if (i+Nx==j && col!=Ny){   // North ~ a_n
              A(i,j) = -dz(slice+1)*dx(row+1)/dyn(col);
            } else if (i-Nx==j && col!=0){    // South ~ a_s
              A(i,j) = -dz(slice+1)*dx(row+1)/dys(col);
            } else if (i+Nx*Ny==j && slice!=Nz){
              A(i,j) = -dx(row+1)*dy(col+1)/dzf(slice); // Front ~ a_f
            } else if (i-Nx*Ny==j && slice!=0) {
              A(i,j) = -dx(row+1)*dy(col+1)/dzb(slice); // Back ~ a_b
            }
          }
        }
      }
private:
  int Nx, Ny, Nz, N;
  double Lx, Ly, Lz, dx0, dy0, dz0;
  vec dx, x, dy, y, dz, z;
  vec xe, xw, ys, yn, zf, zb;
  vec dxe, dxw, dys, dyn, dzf, dzb;
  sp_mat A;
};
