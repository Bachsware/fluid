#pragma once
#include "Grid.h"

template<class Function>
auto divergence(GridData grid, const Function& densityVelocity)->sp_mat{
  int Nx = grid.Nx;
  int Ny = grid.Ny;

  vec dxe = grid.dxe;
  vec dxw = grid.dxw;
  vec dys = grid.dys;
  vec dyn = grid.dyn;

  vec dx  = grid.dx;
  vec dy  = grid.dy;

  int N = Nx*Ny;
  sp_mat A = sp_mat(N,N);
  int row = -1;
  int col = -1;
  for (size_t i = 0; i < N; i++) {
    //Traverse one column at a time, and then iterate rows.
    if (col<Nx-1){
        ++col;
    } else{
        col=0;
    }
    if (col==0)row = row + 1;

    for (size_t j = 0; j < N; j++) {
      if (i+1==j && row!=Nx-1) {   // East ~ a_e
        A(i,j) = dy(col+1)*densityVelocity(xw(row+1),ys(col)).at(0);
      } else if (i-1==j && row!=0){     // West ~ a_w
        A(i,j) = -dy(col+1)/densityVelocity(xw(row-1),ys(col)).at(0);
      } else if (i+Nx==j && col!=Ny-1){   // North ~ a_n
        A(i,j) = dx(row+1)/densityVelocity(xw(row),ys(col+1)).at(1);
      } else if (i-Nx==j && col!=0){    // South ~ a_s
        A(i,j) = -dx(row+1)/densityVelocity(xw(row),ys(col-1)).at(1);
      }
    }
  }
  return A;
}
