#pragma once
#include <armadillo>

using namespace arma;

struct GridData{
  vec dxe,  dxw;
  vec dyn,  dys;
  vec dx,   dy;
  vec x,    y;
  int Nx,   Ny;
};

double max(double a, double b){
  if (a<=b) {
    return b;
  } else return a;
}

class Grid2D{
public:
  Grid2D(double Lx, double Ly, int Nx, int Ny):
    Lx(Lx),
    Ly(Ly),
    Nx(Nx),
    Ny(Ny){
      // Creating dx and dy vectors
      dx = vec(Nx+2); dy = vec(Ny+2);
      dx0 = Lx/double(Nx+1); dy0 = Ly/double(Ny+1);

      dx(0)     = dx0/2.0;
      dx(Nx+1)  = dx0/2.0;
      for (size_t i = 0; i < Nx; i++){dx(i+1) = dx0;}

      dy(0)     = dy0/2.0;
      dy(Ny+1)  = dy0/2.0;
      for (size_t i = 0; i < Ny; i++){dy(i+1) = dy0;}

      // dx.print();
      // dy.print();

      // Creating x and y vectors
      x=vec(Nx); y=vec(Ny);
      x(0) = dx0; y(0)=dy0;
      for (size_t i = 1; i < Nx; i++) {
        x(i) = x(i-1) +dx(i);
      }
      for (size_t i = 1; i < Ny; i++) {
        y(i) = y(i-1) +dy(i);
      }
      // x.print();
      // y.print();

      // Creating vectors with distance to nodes
      dxe = vec(Nx); dxw = vec(Nx); dys = vec(Ny); dyn = vec(Ny);

      dxw(0)    = x(0);
      dxe(Nx-1) = Lx - x(Nx-1);
      for (size_t i = 1; i < Nx; i++) {
        double distance = x(i)-x(i-1);
        dxw(i)  = distance;
        dxe(i-1)= distance;
      }

      dyn(0)    = y(0);
      dys(Ny-1) = Ly - y(Ny-1);
      for (size_t i = 1; i < Ny; i++) {
        double distance = y(i)-y(i-1);
        dyn(i)  = distance;
        dys(i-1)= distance;
      }

      // cout << "dxe" << endl;
      // dxe.print();
      // cout << "dxw" << endl;
      // dxw.print();
      // cout << "dys" << endl;
      // dys.print();
      // cout << "dyn" << endl;
      // dyn.print();

      // Creating vectors with distance to boundaries
      xe = vec(Nx); xw = vec(Nx); ys = vec(Ny); yn = vec(Ny);
      for (size_t i = 0; i < Nx; i++) {
        xw(i) = x(i) - dx(i+1)/2.0;
        xe(i) = x(i) + dx(i+1)/2.0;
      }
      for (size_t i = 0; i < Ny; i++) {
        yn(i) = y(i) - dy(i+1)/2.0;
        ys(i) = y(i) + dy(i+1)/2.0;
      }

      // cout << "xe" << endl;
      // xe.print();
      // cout << "xw" << endl;
      // xw.print();
      // cout << "ys" << endl;
      // ys.print();
      // cout << "yn" << endl;
      // yn.print();

    }

    template<class Function>
    auto calculateTheta(sp_mat lhsOperator, const Function& rhs)->mat{


      mat B(Nx,Ny,fill::zeros);
      for (size_t i = 0; i < Nx; i++) {
        for (size_t j = 0; j < Ny; j++) {
          // Insert Source term
          B(i,j) = rhs(xw(i),xe(i),yn(j),ys(j))*dx(i+1)*dy(j+1); // integral of RHS of poisson eq.

          // Insert boundaries handling
          if (j==0)         B(i,j) += dy(j+1)/dxw(i)*thetaW;
          if (j==Ny-1)      B(i,j) += dy(j+1)/dxe(i)*thetaE;
          if (i==0)         B(i,j) += dx(i+1)/dys(j)*thetaS;
          if (i==Nx-1)      B(i,j) += dx(i+1)/dyn(j)*thetaN;

        }
      }

      // B.print();
      vec b = vectorise(B);
      vec x = spsolve(lhsOperator,b);
      return reshape(x,Nx,Ny);
    }
    sp_mat getCoefficientMatrix(){
      return A;
    }
    double thetaW=0.0, thetaE=0.0, thetaS=0.0, thetaN=0.0;
    vec getX(){return x;}
    vec getY(){return y;}

    GridData getData(){
      GridData data;
      data.dxe = dxe;
      data.dxw = dxw;
      data.dyn = dyn;
      data.dys = dys;
      data.dx  = dx;
      data.dy  = dy;
      data.x   = x;
      data.y   = y;
      data.Nx  = Nx;
      data.Ny  = Ny;

      return data;
    }

  private:
    double max3(double a, double b, double c){
      if (a<=b) {
        if(b<=c){
          return c;
        } else return b;
      } else if(a<=c){
        return c;
      } else return a;
    }

    int Nx, Ny, N;
    double Lx, Ly, dx0, dy0;
    vec dx, x, dy, y, xe, xw, ys, yn, dxe, dxw, dys, dyn;
    sp_mat A;
};
