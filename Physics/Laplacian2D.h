#pragma once
#include "Grid.h"

sp_mat laplacian2D(GridData grid) {
  int Nx = grid.Nx;
  int Ny = grid.Ny;

  vec dxe = grid.dxe;
  vec dxw = grid.dxw;
  vec dys = grid.dys;
  vec dyn = grid.dyn;

  vec dx  = grid.dx;
  vec dy  = grid.dy;

  int N = Nx*Ny;
  sp_mat A = sp_mat(N,N);
  
  int row = -1, col=0;
  for (size_t i = 0; i < N; i++) {
    //Traverse one column at a time, and then iterate rows.
    if (row<Nx-1) {
      ++row;
    } else{
      if(col<Ny){
        ++col;
        row=0;
      }
    }

    for (size_t j = 0; j < N; j++) {
      // cout << "("<< i <<","<< j <<"), row="<<row<<" col="<<col << endl;
      if (i==j) {                       // Diagonal
        A(i,j) = dy(col+1)*(1.0/dxe(row)+1.0/dxw(row))+dx(row+1)*(1.0/dyn(col)+1.0/dys(col));
      } else if (i+1==j && row!=Nx) {   // East ~ a_e
        A(i,j) = -dy(col+1)/dxe(row);
      } else if (i-1==j && row!=0){     // West ~ a_w
        A(i,j) = -dy(col+1)/dxw(row);
      } else if (i+Nx==j && col!=Ny){   // North ~ a_n
        A(i,j) = -dx(row+1)/dyn(col);
      } else if (i-Nx==j && col!=0){    // South ~ a_s
        A(i,j) = -dx(row+1)/dys(col);
      }
    }
  }
  return A;
}
