#pragma once
#include "Grid.h"

template<class Function>
auto convectionDiffusion(GridData data, const Function& vectorField)->sp_mat{
  int Nx = grid.Nx;
  int Ny = grid.Ny;

  vec dxe = grid.dxe;
  vec dxw = grid.dxw;
  vec dys = grid.dys;
  vec dyn = grid.dyn;

  vec dx  = grid.dx;
  vec dy  = grid.dy;

  vec x   = grid.x;
  vec y   = grid.y;

  double F_w, F_e, F_n, F_s;
  double D_w, D_e, D_n, D_s;
  int N = Nx*Ny;
  sp_mat A = sp_mat(N,N);
  int row = -1;
  int col = -1;
  double p =0;
  for (size_t i = 0; i < N; i++) {
    //Traverse one column at a time, and then iterate rows.
    if (row<Nx-1){
        ++row;
    } else{
        row=0;
    }
    if (row==0) ++col;
    for (size_t j = 0; j < N; j++) {
      if (i==j) {                       // Diagonal
        F_w = dy(col+1)*vectorField(x(row)-dxw(row),y(col)).at(0);
        D_w = dy(col+1)/dxw(row);
        F_e = dy(col+1)*vectorField(x(row)+dxe(row),y(col)).at(0);
        D_e = dy(col+1)/dxe(row);
        F_s = dx(row+1)*vectorField(x(row),y(col)-dys(col)).at(1);
        D_s = dx(row+1)/dys(col);
        F_n = dx(row+1)*vectorField(x(row),y(col)+dyn(col)).at(1);
        D_n = dx(row+1)/dyn(row);

        // Naiv
        // A(i,j) = p+D_e-F_e/2.0; // a_e
        // A(i,j)+= D_w-F_w/2.0; // a_w
        // A(i,j)+= D_n-F_n/2.0; // a_n
        // A(i,j)+= D_s-F_s/2.0; // a_s
        // A(i,j)+= F_e - F_w + F_n - F_s;

        // Hybrid
        A(i,j) = max3(-F_e,D_e-F_e/2.0,0); // a_e
        A(i,j)+= max3(F_w,D_w+F_w/2.0,0); // a_w
        A(i,j)+= max3(-F_n,D_n-F_n/2.0,0); // a_n
        A(i,j)+= max3(F_s,D_s+F_s/2.0,0); // a_s
        A(i,j)+= F_e - F_w + F_n - F_s;

        // Upwind
        // A(i,j) = D_e + max3(-F_e,0,0); // a_e
        // A(i,j)+= D_w + max3(F_w,0,0); // a_w
        // A(i,j)+= D_n + max3(-F_n,0,0); // a_n
        // A(i,j)+= D_s + max3(F_s,0,0); // a_s
        // A(i,j)+= F_e - F_w + F_n - F_s;

      } else if (i+1==j && row!=Nx) {   // East ~ a_e
        F_e = dy(col+1)*vectorField(x(row)+dxe(row),y(col)).at(0);
        D_e = dy(col+1)/dxe(row);

        // A(i,j) =  D_e-F_e/2.0;
        A(i,j) = D_e + max3(-F_e,0,0); // a_e
        // A(i,j) =  max3(-F_e,D_e-F_e/2.0,0);
      } else if (i-1==j && row!=0){     // West ~ a_w
        F_w = dy(col+1)*vectorField(x(row)-dxw(row),y(col)).at(0);
        D_w = dy(col+1)/dxw(row);

        // A(i,j) =  D_w+F_w/2.0;
        // A(i,j) = D_w + max3(F_w,0,0); // a_w
        A(i,j) =  max3(F_w,D_w+F_w/2.0,0.0);
      } else if (i+Nx==j && col!=Ny){   // North ~ a_n
        F_n = dx(row+1)*vectorField(x(row),y(col)+dyn(col)).at(1);
        D_n = dx(row+1)/dyn(row);

        // A(i,j) = D_n-F_n/2.0;
        // A(i,j) = D_n + max3(-F_n,0,0); // a_n
        A(i,j) =  max3(-F_n,D_n-F_n/2.0,0);
        // A(i,j) = -dx(row+1)/dyn(col);
      } else if (i-Nx==j && col!=0){    // South ~ a_s
        F_s = dx(row+1)*vectorField(x(row),y(col)-dys(col)).at(1);
        D_s = dx(row+1)/dys(col);
        // A(i,j) = D_n+F_s/2.0;
        // A(i,j) = D_s + max3(F_s,0,0); // a_s
        A(i,j) = max3(F_s,D_s+F_s/2.0,0.0);
        // A(i,j) = -dx(row+1)/dys(col);
      }
    }
  }
  return A;
}
