#pragma once
#include <iostream>
#include "../Physics/Laplacian2D.h"
#include "../Physics/Grid3D.h"

#include "../IO/DataContainer.h"

void doExercise1Part2(){
    using namespace std;
    // Set grid properties
    int Nx = 20;
    int Ny = 20;
    double Lx = 2*datum::pi;
    double Ly = 2*datum::pi;

    // Create grid
    cout << "-- Fluid: Exercise1/part2 \t--> Creating grid" << endl;
    Grid2D grid(Lx,Ly,Nx,Ny);

    // Calculate rhs of poisson equation
    // Assume:  integrate(laplacian(tetha)) = rhs
    // define rhs as you wish:
    auto rhs = [](double xw, double xe, double yn, double ys)->double{
      // return exp(-1.0*(pow(xe-xw - datum::pi,2) + pow(ys-yn - datum::pi,2)));
      return (cos(xe)-cos(xw))*(cos(yn)-cos(ys));
    };

    // Fetch data ~ by passing the rhs function
    cout << "-- Fluid: Exercise1/part2 \t--> Calculating theta" << endl;
    sp_mat laplacian = laplacian2D(grid.getData());
    mat theta = grid.calculateTheta(laplacian,rhs);

    // Save data to file
    cout << "-- Fluid: Exercise1/part2 \t--> Saving data: e1p2data" << endl;
    DataContainer data{};
    data.saveMatrix(theta,"theta");
    data.writeToFile("e1p2data");
}
void doExercise1Part2_3d(){
  using namespace std;

  cout << "-- Fluid: Exercise1_3d/part2 \t--> Creating grid" << endl;
  int    Nx=20,Ny=20,Nz=20;
  double period = 2*datum::pi*2.0;
  double Lx=period*1.0,Ly=period*1.0,Lz=period*1.0;

  Grid3D grid(Lx,Ly,Lz,Nx,Ny,Nz);

  auto rhs = [period](double xw, double xe, double yn, double ys,double zf, double zb)->double{
    return (cos(xe)-cos(xw))*(cos(yn)-cos(ys))*(cos(zf)-cos(zb))*5.0*exp(1.0/period * (xe-xw)*(zf-ys));
  };

  cout << "-- Fluid: Exercise1_3d/part2 \t--> Calculating theta" << endl;

  grid.applyLaplacianOnTheta();

  cube Theta = grid.calculateTheta(rhs);


  cout << "-- Fluid: Exercise1_3d/part2 \t--> Saving data: data3d" << endl;
  DataContainer data{};
  data.saveCube(Theta);
  data.writeToFile("data3d");
}
