#pragma once
#include <iostream>
#include "../Physics/Laplacian2D.h"
#include "../IO/DataContainer.h"

void doExercise2Part1(){
  using namespace std;
  // Set grid properties
  int Nx = 30;
  int Ny = 30;
  double Lx = 1.0;
  double Ly = 1.0;

  // Create grid
  cout << "-- Fluid: Exercise2/part1 \t--> Creating grid" << endl;
  Grid2D grid(Lx,Ly,Nx,Ny);
  vec x = grid.getX();
  vec y = grid.getY();

  // Set boundaries
  grid.thetaE = 0.0;
  grid.thetaW = 100.0;
  grid.thetaN = 100.0;
  grid.thetaS = 0.0;

  // Source field
  auto rhs = [](double xw, double xe, double yn, double ys)->double{
    return 0.0;
  };

  // velocity field * densityField = combinedField
  auto combinedField = [](double x, double y)->vec{
    return {2,2};
  };

  cout << "-- Fluid: Exercise2/part1 \t--> Creating operators" << endl;
  // sp_mat lhs = grid.laplacian()-grid.divergence(combinedField);
  // sp_mat lhs = grid.convectionDiffusion(combinedField);
  sp_mat lhs = laplacian2D(grid.getData());

  cout << "-- Fluid: Exercise2/part1 \t--> Solving theta" << endl;
  mat theta = grid.calculateTheta(lhs,rhs);
  // (lhs*vectorise(theta)).print();

  // Save data to file
  string saveFile = "e2p1_data";

  cout << "-- Fluid: Exercise2/part1 \t--> Saving data: " << saveFile << endl;

  DataContainer data{};
  data.saveMatrix(theta,"theta");
  data.saveVector(x,"x");
  data.saveVector(y,"y");

  data.writeToFile(saveFile);
}
