#include <iostream>

#include <algorithm>
#include <functional>

#include "Analysis/Exercise1.h"
#include "Analysis/Exercise2.h"

int main(int argc, char const *argv[]) {
  using namespace std;

  // Exercise 1
  // doExercise1Part2();
  // doExercise1Part2_3d();

  // Exercise 2
  doExercise2Part1();

  cout << "-- Fluid: Done" << endl;
  return 0;
}
