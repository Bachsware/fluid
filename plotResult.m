
close all
clear all
clc

addpath './Data'
fileName = '2dProblem.dat'

data = dlmread(fileName,' ',1,1);
N = length(data)+2;
theta = zeros(N,N-1);

theta(2:end-1,2:end) = data;

x = linspace(0,2*pi,N-1);
y = linspace(0,2*pi,N);

figure
surf(x,y,theta)
xlabel 'x'
ylabel 'y'
zlabel '\theta'
axis tight